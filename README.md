# mugui-sql

#### 介绍
超轻量级数据库框架，非hibermate（如：MyBatis）系列,使用场景：中小型项目，快速开发，简单，无学习成本。

* 支持多数据库。
* 支持bean与数据库表双向生成。
* 支持json与bean之间快速装换。
* 支持不同bean之间的快速转换与装载。


#### 安装教程
### 3 sql

```
<dependency>
  <groupId>cn.net.mugui</groupId>
  <artifactId>sql</artifactId>
  <version>{version}</version>
</dependency>

```
#### 使用说明

1.  新建实体类（或使用使用工具自动生成）
  
```
@Getter
@Setter
@Accessors
@SQLDB(TABLE = "user",KEY = "user_id")
public class UserBean  extends JsonBean{
	@SQLField(AUTOINCREMENT = true,PRIMARY_KEY = true)//自增长，主键
	private Integer user_id;
	
	@SQLField(NULL=false)//不允许为空
	private String user_name;
	
}
```
      



2.  查询

```
                /**
		 * {@link SqlModeApi} 可由springboot 注入
		 */
		 SqlModel sqlModel = new SqlModel();//可由其他类继承
		 UserBean userBean = new UserBean();
		 userBean.setUser_id(1);
	        {//生成User表
			sqlModel.createTable(UserBean.class);
		}
		 {//通过主键查询
				UserBean userBean2 = sqlModel.get(userBean);
		 }
		 {
			 //通过条件查询,所有值都自动等于userbean中赋予的值
			 userBean.setUser_name("holle");
			UserBean select = sqlModel.select(userBean);
		 }
		 {
			 //通过条件查询,得到最后一个结果
			 userBean.setUser_name("holle");
			UserBean select = sqlModel.selectDESC(userBean);
		 }
		 {
			 //通过条件查询
			 userBean.setUser_name("holle");
			 List<UserBean> selectList = sqlModel.selectList(userBean);
		 }
		 {
			 //通过条件查询
			 userBean.setUser_name("holle");
			  JSONArray selectArrayDESC = sqlModel.selectArrayDESC(userBean);
		 }
		 {
			 //多对象查询
			 userBean.setUser_name("holle");
			  UserBean select = sqlModel.select(UserBean.class,userBean,new DefaultJsonBean());
		 }
```

3.  对象装换（模糊对象与对象，json与实体类，对象与数据库之间的界限）
		
```
 {
			 //通过json或者json字符串快速生成对应对象
			 UserBean newBean = JsonBean.newBean(UserBean.class,"{\"user_id\":1}");
			 
			 JSONObject object=new JSONObject();
			 object.put("user_id",1);
			 UserBean newBean2 = UserBean.newBean(UserBean.class,object);
			 
			 //将UserBean对象转换为DefaultJsonBean对象
			 DefaultJsonBean newBean3 = UserBean.newBean(DefaultJsonBean.class,newBean2);
			 //得到源数据 
			 JSONObject jsonObject = newBean3.get();
			 //得到当前类json数据
			 JSONObject json = newBean3.toJson();
			 
		 }
```

更多方法查看api,或访问示例项目：
