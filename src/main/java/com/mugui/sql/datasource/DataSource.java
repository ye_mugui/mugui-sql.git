package com.mugui.sql.datasource;

import com.mugui.Mugui;
import com.mugui.sql.DBConf;

public interface DataSource extends Mugui {
	javax.sql.DataSource getDataSource();

	javax.sql.DataSource getDataSource(DBConf conf);

	javax.sql.DataSource getDataSource(String url);
}
