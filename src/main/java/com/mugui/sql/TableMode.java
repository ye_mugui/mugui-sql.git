package com.mugui.sql;

import java.io.Serializable;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.mugui.bean.DefaultJsonBean;

/**
 * 一个完整的表数据装载器
 * 
 * @author 木鬼
 *
 */
public final class TableMode implements Serializable {
	private static final long serialVersionUID = 8606735714775342730L;
	private int len = 0;
	private ArrayList<JSONObject> Tablemode = null;
	private ArrayList<String> column_name = null;
	private String tableName;

	public String getTableName() {
		return tableName;
	}

	public TableMode() {

	}

	public TableMode(ResultSet rs) {
		try {
			this.len = rs.getMetaData().getColumnCount();
			this.tableName = rs.getMetaData().getTableName(1);
			this.Tablemode = new ArrayList<JSONObject>(this.len);
			JSONObject v = new JSONObject();
			if (rs.next()) {
				this.column_name = new ArrayList<>();
				for (int i = 1; i <= this.len; ++i) {
					this.column_name.add(rs.getMetaData().getColumnLabel(i));
					Object s = rs.getObject(i);
					v.put(this.column_name.get(i - 1), s);
				}
				this.Tablemode.add(v);
			}
			while (rs.next()) {
				v = new JSONObject();
				for (int i = 1; i <= this.len; ++i) {
					Object s = rs.getObject(i);
					v.put(this.column_name.get(i - 1), s);
				}
				this.Tablemode.add(v);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (rs != null)
					rs.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	public int getColumnCount() {
		return this.len;
	}

	public int getRowCount() {
		return this.Tablemode.size();
	}

	public String getValueAt(int arg0, int arg1) {
		JSONObject taStrings = null;
		try {
			taStrings = this.Tablemode.get(arg0);
		} catch (Exception e) {
			return null;
		}
		String s = null;
		try {
			s = taStrings.get(column_name.get(arg1)).toString();
		} catch (Exception e) {
			return s;
		}
		return s;
	}

	public String getColumnName(int column) {
		if (this.column_name == null)
			return null;
		return ((String) this.column_name.get(column));
	}

	public JSONObject getRowData(int row) {
		return this.Tablemode.get(row);
	}

	@Override
	public String toString() {
		String string = "";
		for (int i = 0; i < getColumnCount(); i++) {
			string += getColumnName(i) + "\t";
		}
		string += "\n";
		for (int i = 0; i < getRowCount(); i++) {
			for (int j = 0; j < getColumnCount(); j++) {
				string += getValueAt(i, j) + "\t";
			}
			string += "\n";
		}
		return string;
	}

	public JSONArray getData() {
		JSONArray jsonArray = new JSONArray();
		jsonArray.addAll(this.Tablemode);
		return jsonArray;
	}

	public Iterator<JSONObject> iterator() {
		return Tablemode.iterator();
	}

	/**
	 * 改变表数据
	 * 
	 * @author 木鬼
	 * @param aValue
	 * @param rowIndex
	 * @param columnIndex
	 */
	public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
		Tablemode.get(rowIndex).put(column_name.get(columnIndex), aValue);
	}

	/**
	 * 删除一行
	 * 
	 * @author 木鬼
	 * @param row
	 */
	public void removeRow(int row) {
		Tablemode.remove(row);
	}

	/**
	 * 添加一行
	 * 
	 * @author 木鬼
	 * @param values
	 */
	public void addRow(String[] values) {
		DefaultJsonBean object = new DefaultJsonBean();
		for (int i = 0; i < values.length; i++) {
			object.set(column_name.get(i), values[i]);
		}
		Tablemode.add(object.get());
	}

	/**
	 * 字段名列表
	 * 
	 * @author 木鬼
	 * @return
	 */
	public ArrayList<String> getColumnNames() {
		return column_name;
	}
	
	public ArrayList<String> setColumnNames(ArrayList<String> column_name){
		this.column_name=column_name;
		return column_name;
	}
	
}