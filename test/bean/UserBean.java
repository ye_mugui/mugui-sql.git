package bean;

import com.mugui.bean.JsonBean;
import com.mugui.sql.SQLDB;
import com.mugui.sql.SQLField;
import com.mugui.sql.SqlModel;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

@Getter
@Setter
@Accessors
@SQLDB(TABLE = "user",KEY = "user_id")
public class UserBean  extends JsonBean{
	@SQLField(AUTOINCREMENT = true,PRIMARY_KEY = true)//自增长，主键
	private Integer user_id;
	
	@SQLField(NULL=false)//不允许为空
	private String user_name;
	
}
