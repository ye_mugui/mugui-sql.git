package sql;

import java.util.List;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.mugui.bean.DefaultJsonBean;
import com.mugui.bean.JsonBean;
import com.mugui.sql.SqlModeApi;
import com.mugui.sql.SqlModel;

import bean.UserBean;

public class SelectTest {
	
	public static void main(String[] args) {
		/**
		 * {@link SqlModeApi} 可由springboot 注入
		 */
		 SqlModel sqlModel = new SqlModel();//可由其他类继承

		{//生成User表
			sqlModel.createTable(UserBean.class);
		}
		
		 UserBean userBean = new UserBean();
		 userBean.setUser_id(1);
		 {//通过主键查询
				UserBean userBean2 = sqlModel.get(userBean);
		 }
		 {
			 //通过条件查询,所有值都自动等于userbean中赋予的值
			 userBean.setUser_name("holle");
			UserBean select = sqlModel.select(userBean);
		 }
		 {
			 //通过条件查询,得到最后一个结果
			 userBean.setUser_name("holle");
			UserBean select = sqlModel.selectDESC(userBean);
		 }
		 {
			 //通过条件查询
			 userBean.setUser_name("holle");
			 List<UserBean> selectList = sqlModel.selectList(userBean);
		 }
		 {
			 //通过条件查询
			 userBean.setUser_name("holle");
			  JSONArray selectArrayDESC = sqlModel.selectArrayDESC(userBean);
		 }
		 {
			 //多对象关联查询
			 userBean.setUser_name("holle");
			  UserBean select = sqlModel.select(UserBean.class,userBean,new DefaultJsonBean());
		 }
		 {
			 //通过json或者json字符串快速生成对应对象
			 UserBean newBean = JsonBean.newBean(UserBean.class,"{\"user_id\":1}");
			 
			 JSONObject object=new JSONObject();
			 object.put("user_id",1);
			 UserBean newBean2 = UserBean.newBean(UserBean.class,object);
			 
			 //将UserBean对象转换为DefaultJsonBean对象
			 DefaultJsonBean newBean3 = UserBean.newBean(DefaultJsonBean.class,newBean2);
			 //得到源数据 
			 JSONObject jsonObject = newBean3.get();
			 //得到当前类json数据
			 JSONObject json = newBean3.toJson();
			 
		 }
		 
		 
	}
}
